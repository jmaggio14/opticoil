clear; close all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         SET EVERY SESSION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RIGID BODY AND MARKER IDS
HELMET_ID = 2;
SIDE_HELMET_COIL_ID = 3;
BACK_HELMET_COIL_ID = 5;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                         modifiable
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%% ANALOG CHANNELS
%input
DEVICE = 'Dev5'; % this is the name of the DAQ - can be retrieved with daq.getDevices
REF12 = 'ai0'; % channel for 12K reference coil
REF16 = 'ai1'; % channel for 16K reference coil
REF20 = 'ai2'; % channel for 20K reference coil
LEFT_EYE_COIL = 'ai3'; % channel for the left eye coil
RIGHT_EYE_COIL = 'ai4'; % channel for the right eye coil
SIDE_HEAD_COIL = 'ai5'; % channel for the side head coil
BACK_HEAD_COIL = 'ai6'; % channel for the back head coil

% output
CAMERA_TRIGGER_OUT = 'ao0'; % channel for camera trigger

%%%% DIGITAL CHANNELS
CAMERA_EXPOSURE = 'port0/Line0';

EYE_BUTTON    = 'port0/Line5';
START_BUTTON  = 'port0/Line4';
STOP_BUTTON   = 'port0/Line3';
CAL_BUTTON    = 'port0/Line2';
USER_BUTTON1  = 'port0/Line6';
USER_BUTTON2  = 'port0/Line1';  
USER_BUTTON3  = 'port0/Line7';


SAMPLE_RATE = 80e3; % samples / second, for both input and output channels
CALLBACK_LIMIT = round(SAMPLE_RATE / 1, 0); % divisor defines callbacks per second
EXTERNAL_TRIGGER_TIMEOUT = 20; % seconds until matlab gives up waiting for motive to trigger it
TRIGGER_PORT = '/PFI0';
VIDEO_RECORD_FRAMERATE = 30; % necessary due to slow webcam (and matlab...)
VIDEO_RECORD_RESOLUTION = '640x480';

% Optitrack Variables
FRAME_RATE = 240;%197.3; % frames per second
TIMEOUT = FRAME_RATE * 15; % in frames - typically defined as FRAME_RATE * seconds
THREAD_COMM_START_FILE = 'THREAD_COMM_START-do_not_delete_while_matlab_is_running';
THREAD_COMM_STOP_FILE = 'THREAD_COMM_STOP-do_not_delete_while_matlab_is_running';


%%%%%%%%%%%%%%%%%%% FOR GALVO CALIBRATION ONLY %%%%%%%%%%%%%%%
GAVLO_READ = 'ai7'; % 
GALVO_DRIVER_TRIGGER = 'ao1'; % must be analog output 
GALVO_SAMPLE_RATE = 10e3; % samples/second
GALVO_OSCILLATION_FREQUENCY = 1; %HZ - NOTE: less than 1 Hz is not possible
GALVO_DRIVER_AMPLITUDE = 10; % Volts
GALVO_CALLBACK_LIMIT = round(GALVO_SAMPLE_RATE,0); % samples 
GALVO_VOLTS_PER_ARCMIN = 42.5e-3; % volts/arcmin



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                       DO NOT MODIFY BELOW THIS LINE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% start of experiment
START_TIME = datestr( datetime() );
POSIX_START = posixtime( datetime() );

% sound that matlab will beep with
BEEP_SOUND = sin( 2*pi*8e3 .* linspace(0, 1, 5e2) );
% DEFINE GLOBAL VARIABLES
% these will be fed into optitrackrecord.m and coilrecord.m
SESSION_NAME = input(' would you like to call this session?\n', 's');

DATE_DIR = ['opus/fieldCalibrate/data/', date, ];
UPLOAD_DIR = [DATE_DIR, '/', SESSION_NAME];

% create output directory for our data filesz
DIRNAME = fullfile(date, SESSION_NAME);
[status, msg] = mkdir(DIRNAME);

if ~status
    error(['failed to create output directory: ', msg]);
end

COIL_FILENAME = fullfile(DIRNAME, [SESSION_NAME, '_coil.csv']);
TAGS_FILENAME = fullfile(DIRNAME, [SESSION_NAME, '_tags.csv']);
SETTINGS_FILENAME = fullfile(DIRNAME, [SESSION_NAME, '_settings.mat']);
VIDEO_FILENAME = fullfile(DIRNAME, [SESSION_NAME,'_video.avi']);

% ---------------------- optitrack record variables ----------------------
% these have to be defined in the form of a struct for the batch function
OPTI_WORKSPACE = struct;
OPTI_WORKSPACE.TIMEOUT_IN_FRAMES = TIMEOUT;
OPTI_WORKSPACE.THREAD_COMM_START_FILE = THREAD_COMM_START_FILE;
OPTI_WORKSPACE.THREAD_COMM_STOP_FILE = THREAD_COMM_STOP_FILE;
OPTI_WORKSPACE.OPTITRACK_FILENAME = fullfile(DIRNAME, [SESSION_NAME, '_opti.json']);
OPTI_WORKSPACE.TAKE_NAME = SESSION_NAME;

% ---------------------- video record variables ----------------------
VIDEO_WORKSPACE = struct;
VIDEO_WORKSPACE.VIDEO_FILENAME = VIDEO_FILENAME;
VIDEO_WORKSPACE.THREAD_COMM_STOP_FILE = THREAD_COMM_STOP_FILE;
VIDEO_WORKSPACE.VIDEO_RECORD_FRAMERATE = VIDEO_RECORD_FRAMERATE;
VIDEO_WORKSPACE.VIDEO_RECORD_RESOLUTION = VIDEO_RECORD_RESOLUTION;

% setup the thread communication file
if isfile(THREAD_COMM_START_FILE)
    delete(THREAD_COMM_START_FILE);
end
if isfile(THREAD_COMM_STOP_FILE)
    delete(THREAD_COMM_STOP_FILE);
end

DAQ_INFO =  'ERROR: unable to find device information';
d = daq.getDevices();
for i = 1:numel(d)
  d_info = get( d(i) );
  d_subsystems = d(i).Subsystems(1);
  if d_info.ID == DEVICE
    DAQ_INFO = d_info;
  end
end

% Save this workspace to the file for later debugging
save(SETTINGS_FILENAME);
