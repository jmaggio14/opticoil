function [] = plotdata(src,event, gavlo_idx, cam_exp_idx, VOLTS_PER_ARCMIN)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes hereWW
subplot(1,1,1);
cla();

angle = event.Data(:,gavlo_idx) / VOLTS_PER_ARCMIN;
plot(event.TimeStamps, angle);
disp('plotting...');
xlabel('Time (seconds)');
ylabel('Angle (arcmin)');
legend('galvo position');
end

