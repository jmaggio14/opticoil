daq_session = daq.createSession('ni');

daq_session.ExternalTriggerTimeout = 5;
daq_session.Rate = 500;
daq_session.NotifyWhenDataAvailableExceeds = 500;
daq_session.IsContinuous = 1;

[idc_channel_obj, idc_column_idx] = daq_session.addAnalogOutputChannel('Dev5','ao1','Voltage');
[idc_channel_obj2, idc_column_idx2] = daq_session.addAnalogOutputChannel('Dev5','ao0','Voltage');
trigger_wave1 = square( 2*pi*3 * linspace(0, 1, daq_session.Rate) );
trigger_wave1 = trigger_wave1';

trigger_wave2 = -trigger_wave1*2;

trigger_wave = [trigger_wave1, trigger_wave2];

daq_session.queueOutputData(trigger_wave);
daq_session.addlistener('DataRequired', @(src,event) src.queueOutputData(trigger_wave));


daq_session.startBackground();