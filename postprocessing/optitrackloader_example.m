%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              How to load in aand work with Optitrack Data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Before you begin, you'll need to know the "Streaming ID" of your rigid
% body or bodies. And possibly also your desired Marker's Active ID. the
% following code can also be used to track the location of a point with a
% known offset from the rigid body
% 
% Data is saved in a different way than before
% To make it easier to work with, I've written an object to load it in for
% you. However, I'll show you how to access the raw data if you'd prefer
% later in this script
close all; clear;
addpath('..');
%we'll define some example variables
HELMET_ID = 6; % rigid body id
COIL1_MARKER_ID_ON_HELMET = 1; % active id for the first coil on the helmet
COIL2_MARKER_ID_ON_HELMET = 2; % active id for the second coil on the helmet

% 1) Create the loader object
loader = OptitrackLoader('Z:\fieldCalibrate\data\09-Sep-2019\run4\run4_opti.json');

% 2) fetch the location and orientation of your desired rigid body for every frame (you 
% need the rigid body's id)
[frame_nums, helmet_locations] = loader.getRigidBodyLoc(HELMET_ID);
[frame_nums, helmet_orientations] = loader.getRigidBodyQuat(HELMET_ID);

% 3) fetch the location of a marker in all frames
[frame_nums, coil1_locations] = loader.getMarkerLoc(HELMET_ID, COIL1_MARKER_ID_ON_HELMET);
[frame_nums, coil2_locations] = loader.getMarkerLoc(HELMET_ID, COIL2_MARKER_ID_ON_HELMET);

% 4) fetch the location of a relative offset from the rigid body
% for example, if we know the eye is located at [-1;-1;-1] relative to the
% helmet (in the helmet's coordinate system). we can determine the location
% of the eye in every single frame frame
% X,Y,Z coordinates here should be a 3x1 vector
[frame_nums, eye_locations] = loader.getRigidBodyOffsetLoc(HELMET_ID, [-1;-1;-1]);


% 5) fetch the timestamps or other metadata for every frame
% each frame is a struct containing the following fields:
%     {'markerSetCount'       }
%     {'unlabeledMarkersCount'}
%     {'rigidBodyCount'       }
%     {'skeletonCount'        }
%     {'labeledMarkerCount'   }
%     {'timecode'             }
%     {'timecodeSub'          }
%     {'timestamp'            }
%     {'isRecording'          }
%     {'trackedModelsChanged' }
%     {'rigid_bodies'         } - all rigid body data

% a. - fetch an individual frame
frame = loader.getFrame(1); % fetch frame one
timestamp = frame.timestamp;

% b. - fetch all frames
% this is a struct containing all frames, where the field is 'x<frame_number>'
frames = loader.getFrames();

% 6) get the relative location of all markers within it's rigid body
% coordinate system. marker locations is [X;Y;Z]
% this returns a struct
marker_offsets = loader.getMarkerOffsets(HELMET_ID);


% EXAMPLE
% let's plot the location of a rigid body and the coil marker 
figure;
grid on;
xlabel('X (m)')
ylabel('Y (m)')
zlabel('Z (m)')
view(45, 10);
axis([-2,2,-2,2,-2,2]);

% starting a few seconds in when the subject is off the
% bitebar
for i = 6000:60:length(frame_nums)
    
    frame_id = frame_nums(i);
    title(['frame',num2str(frame_id)]);
    cla();
    hold on;
    
    % plot the rigid body location
    rb_pos = helmet_locations(i,:);
    plot3(rb_pos(1), rb_pos(2), rb_pos(3), 'ko');
    % plot orientation
    orientation = helmet_orientations(i,:);
    rb_quat = quaternion( orientation(2),orientation(3),orientation(4),orientation(1) );
    rb_dir = rb_quat.EulerAngles('xyz');
    quiver3(rb_pos(1), rb_pos(2), rb_pos(3),rb_dir(1),rb_dir(2),rb_dir(3),'b');
    
    % plot the coil locations
    coil1_pos = coil1_locations(i,:);
    coil2_pos = coil2_locations(i,:);
    plot3(coil1_pos(1), coil1_pos(2), coil1_pos(3),'g+');
    plot3(coil2_pos(1), coil2_pos(2), coil2_pos(3),'m+');
    
%     
    hold off
    % pause a quarter of a second   
    pause(0.5);
end


% RAW DATA
% if you desire it, the raw data that I saved is available as a stuct
% it's a very large heirarchical struct
raw_data = loader.data;


