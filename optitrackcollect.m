
if ~(exist('OPTI_WORKSPACE') == 1)
    SESSION_NAME = input('what is the name of the session you will be recording? (keep it the same as the capture name)\n','s');
    date_dir = ['opus/fieldCalibrate/data/', date];
    upload_dir = [date_dir, '/', SESSION_NAME];
    OPTITRACK_FILENAME = fullfile(date, SESSION_NAME, [SESSION_NAME, '_opti.csv']);
    disp(['data will be saved to ', OPTITRACK_FILENAME]);
    TIMEOUT = 240 * 30;
else
    OPTITRACK_FILENAME = OPTI_WORKSPACE.OPTITRACK_FILENAME;
end
    
cmd = ['python .\PythonClient\OptitrackRecord.py ',OPTITRACK_FILENAME];    
disp('running python client...');
system(cmd);

if ~(exist('OPTI_WORKSPACE') == 1)

    % upload the file to opus
    ftpobj = ftp('128.151.171.182','aplab','Qebgcsft2018');
    ftpobj.cd(UPLOAD_DIR);
    disp(['copying optitrack data to ',UPLOAD_DIR]);
    ftpobj.mput(OPTITRACK_FILENAME);
    ftpobj.close();
else
    disp('unable to upload file to opus without access to collection settings');
    
end