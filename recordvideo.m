if ~(exist('VIDEO_FILENAME') == 1)
    VIDEO_FILENAME = 'test.avi';
    THREAD_COMM_STOP_FILE = 'THREAD_COMM_STOP-do_not_delete_while_matlab_is_running';
    VIDEO_RECORD_FRAMERATE = 30;
    VIDEO_RECORD_RESOLUTION = '640x480';
end


cam = webcam;
cam.Resolution = VIDEO_RECORD_RESOLUTION;

vid_writer = VideoWriter(VIDEO_FILENAME);
vid_writer.FrameRate = VIDEO_RECORD_FRAMERATE;
open(vid_writer);


start_t = posixtime( datetime( 'now' ) );
n_frames = 0;
while true
    img = snapshot(cam);
    n_frames = n_frames + 1;
    
    writeVideo(vid_writer, img);
   % don't end recording until we get a message from another thread
    if isfile(THREAD_COMM_STOP_FILE)
        break
    end
end

duration = posixtime( datetime('now') ) - start_t;
capture_framerate = n_frames / duration;
disp(['capture framerate: ', num2str(capture_framerate)]);
disp(['recording framerate: ', num2str(vid_writer.FrameRate)]);

close(vid_writer);
clear cam;