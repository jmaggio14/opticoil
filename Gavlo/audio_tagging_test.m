
daq_session = daq.createSession('ni');

daq_session.ExternalTriggerTimeout = 5;
daq_session.Rate = 1e4;
daq_session.NotifyWhenDataAvailableExceeds = round(daq_session.Rate,0);
daq_session.IsContinuous = 1;

[~, ~] = addAnalogInputChannel(daq_session,'Dev5','ai0','Voltage');
[~, audio_tag] =  addDigitalChannel(daq_session,'Dev5','port2/Line1','InputOnly');

idxs = struct('audio',audio_tag);

lh_audio = daq_session.addlistener('DataAvailable',...
              @(src,event) plotaudio(src,event,idxs));

daq_session.startBackground();