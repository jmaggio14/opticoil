function [] = plotaudio(src,event,idxs)
  data = event.Data(:,idxs.audio);
  plot(event.TimeStamps, data);
end