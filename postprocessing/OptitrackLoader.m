% FUNCTIONS TO ADD
% 1) get all markers positions on rigid body (for plotting ease)
% 2) look up position and orientation by frame easier (no search)
% 3) add interpolation for missed frames.
% 4)


classdef OptitrackLoader
    % loader = OptitrackLoader(filename)
    %
    % % Get the xyz location of a marker on the rigid body
    % marker_loc = loader.getMarkerLoc(rigid_body_id, marker_id)
    %
    % % get data from an arbitrary offset from the rigid body
    % eye_offset_from_helmet = [-1,-1,-1] %mm
    % eye_loc = loader.getRigidBodyOffsetLoc(rigid_body, eye_offset_from_helmet)
    
    properties
        data
        frame_nums
    end
    
    methods
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj = OptitrackLoader(filename)
            %OptitrackLoader Construct an instance of this class
            %   Detailed explanation goes here
            encoded = fileread(filename);
            obj.data = jsondecode(encoded);
            obj.frame_nums = obj.getFrameNums();
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function frame_nums = getFrameNums(obj)
            % get all frame ids that we collected
            frame_nums = fieldnames(obj.data.frames);
            for i = 1:length(frame_nums)
                frame_nums{i} = str2double( strip( frame_nums{i}, 'x') );
            end
            frame_nums = reshape(cell2mat(frame_nums),[length(frame_nums),1]);
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function frames_metadata = getFrames(obj)
            % returns a struct which contains all metadata about each frame
            % frame numbers are prefixed with 'x'
            frames_metadata = obj.data.frames;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function frame = getFrame(obj, frame_num)
            frame = getfield(obj.data.frames, ['x', num2str(frame_num)] );
                        
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function offsets = getMarkerOffsets(obj, rigid_body)
           offsets = getfield(obj.data.relative_marker_position, ['x', num2str(rigid_body)] );
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [frame_nums, rigid_body_loc] = getRigidBodyLoc(obj, rigid_body)
            % fetch the rigid body location for all frames
            rigid_body_loc = zeros( [length(obj.frame_nums),3] );
            % iterate through all frames and fetch the corresponding rigid
            % body position
            for i = 1:length(obj.frame_nums)
                % fetch the frame
                frame_id = obj.frame_nums(i);
                frame = obj.getFrame(frame_id);
                
                rb = getfield(frame.rigid_bodies,['x',num2str(rigid_body)]);
                rigid_body_loc(i,:) = rb.pos;
            end
            frame_nums = obj.frame_nums;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [frame_nums, rigid_body_quat] = getRigidBodyQuat(obj, rigid_body)
            % fetch the rigid body orientation for all frames
            rigid_body_quat = zeros( [length(obj.frame_nums),4] );
            % iterate through all frames and fetch the corresponding rigid
            % body rotation
            for i = 1:length(obj.frame_nums)
                % fetch the frame
                frame_id = obj.frame_nums(i);
                frame = obj.getFrame(frame_id);
                
                rb = getfield(frame.rigid_bodies,['x',num2str(rigid_body)]);
                rigid_body_quat(i,:) = rb.quat;
            end
            frame_nums = obj.frame_nums;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [frame_nums,marker_loc] = getMarkerLoc(obj, rigid_body, marker_id)
            % do transformation on marker offset to get xyz
            
            % fetch the xyz of the marker in the rigid body coordinate
            % frame
            offsets = obj.getMarkerOffsets(rigid_body);
            relative_xyz = getfield(offsets,['x',num2str(marker_id)]);
                        
            [frame_nums,marker_loc] = obj.getRigidBodyOffsetLoc(rigid_body, relative_xyz);
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [frame_nums, offset_loc] = getRigidBodyOffsetLoc(obj,rigid_body,offset)
            % getRigidBodyOffsetLoc: calculate the location of the a point
            % offset from the rigid bodies coordinate system for all frames. 
            % Returns a matrix where every row is [frame_id, global_x, global_y, global_z]
                       
            % fetch rigid body orientation and positions
            [~,rigid_body_loc] = obj.getRigidBodyLoc(rigid_body);
            [~,quat] = obj.getRigidBodyQuat(rigid_body);
            rigid_body_quat = quaternion( quat(2),quat(3),quat(4),quat(1) );
            
            % copy offset rb_offset matrix for every frame
            offset_repeated = repmat(offset',[length(obj.frame_nums),1]);
            
            % apply the quaternion from every frame to the offset
            rotated = rigid_body_quat.RotateVector(offset_repeated);
            % add the global offset of the rigid body
            transformed = rotated + rigid_body_loc;
            
            % merge the frame numbers into the output matrix
            offset_loc = transformed;
            frame_nums = obj.frame_nums;
        end           
    end
end 

