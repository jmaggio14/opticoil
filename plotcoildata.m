function [] = plotcoildata(src,event,idxs,beep)
%plotcoildata: saves coil data to a file
%   src   : the source of the daq data
%   event : the data from the data colection event
%   idxs : indices for each channel in
%              in the form of {ref1_idx,ref2_idx,ref3_idx,coil_idx}
left_eye_data = event.Data(:,idxs.left_eye);
right_eye_data = event.Data(:,idxs.right_eye);
side_coil_data = event.Data(:,idxs.side_head);
back_coil_data = event.Data(:,idxs.back_head);


cla();
text(.3, .75, [num2str( round(event.TimeStamps(end),1) ),' seconds'],'FontSize', 30);
text(.05, .45, ['[left eye] min: ', num2str( round(min(left_eye_data),3) ),'V | max: ', num2str( round(max(left_eye_data),3) ), 'V'],'FontSize', 14);
text(.05, .35, ['[right eye] min: ', num2str( round(min(right_eye_data),3) ),'V | max: ', num2str( round(max(right_eye_data),3) ), 'V'],'FontSize', 14);
text(.05, .25, ['[side of head] min: ', num2str( round(min(side_coil_data),3) ),'V | max: ', num2str( round(max(side_coil_data),3) ), 'V'],'FontSize', 14);
text(.05, .15, ['[back of head] min: ', num2str( round(min(back_coil_data),3) ),'V | max: ', num2str( round(max(back_coil_data),3) ), 'V'],'FontSize', 14);

% % eye event
% if any( event.Data(:,idxs.eyeprobe) )
%     sound(beep);
% end
% % start/stop event
% if any( event.Data(:,idxs.start_stop) )
%     sound(beep);
% end
% % calibration event
% if any( event.Data(:,idxs.cal) )
%     sound(beep);
% end
% 
% 
% % user event 1
% if any( event.Data(:,idxs.user1) )
%     sound(beep);
% end
% % user event 2
% if any( event.Data(:,idxs.user2) )
%     sound(beep);
% end
% % user event 3
% if any( event.Data(:,idxs.user3) )
%     disp('user3 event detected');
% end
% % user event 4
% if any( event.Data(:,idxs.user4) )
%     disp('user4 event detected');
% end

set(gca,'xtick',[])
set(gca,'xticklabels',[])
set(gca,'ytick',[])
set(gca,'yticklabels',[])
end
