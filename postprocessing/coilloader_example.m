% NOTE:
% Coil data can use up an enormous amount of memory. Unless you are working
% with a small file. It may be necessary to use one of the engineering
% workstations OR the Argus Computing Server.



% %globals
SETTINGS_FILENAME = 'C:\Users\APLab\Documents\MATLAB\opticoil\17-Sep-2019\exp_test7\exp_test7_settings.mat';
TAGS_FILENAME = 'C:\Users\APLab\Documents\MATLAB\opticoil\17-Sep-2019\exp_test7\exp_test7_tags.csv';
DATA_FILENAME = 'C:\Users\APLab\Documents\MATLAB\opticoil\17-Sep-2019\exp_test7\exp_test7_coil.csv';

% create the CoilLoader object to load in and compute basic coil and tag
% data
coil_loader = CoilLoader(DATA_FILENAME, TAGS_FILENAME, SETTINGS_FILENAME);

% Get a struct that represents all capture settings
% this contains information such as frame_rate, filenames, and the channels
% used for each data stream
session_settings = coil_loader.getSettings();

% fetch the marker and rigid body ids
helmet_id = session_settings.HELMET_ID;
side_helmet_coil_id = session_settings.SIDE_HELMET_COIL_ID;
back_helmet_coil_id = session_settings.BACK_HELMET_COIL_ID;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GET RAW VOLTAGE DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get the reference coil voltages
% ROWS are samples
[ref12k, ref16k, ref20k] = coil_loader.getReferenceCoils();

% get eye coil data
% ROWS are samples
[left_eye_voltage, right_eye_voltage] = coil_loader.getEyeCoils();

% get helmet coils
% ROWS are samples
[side_helmet_voltage, back_helmet_voltage] = coil_loader.getHelmetCoils();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMPUTE EVENT TIMES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sanity check the number of frames we counted
num_frames = coil_loader.computeNumFrames();


% get the frames where the left and right eyes were touched by the eyeprobe
% ASSUMES the left eye was touched first!
[left_eyeprobe_frame, right_eyeprobe_frame] = coil_loader.computeEyeprobeFrames();

% compute the optitrack frames where experiment trials started and ended
% this is a matrix structed as follows:
% [trial1_start_frame, trial1_stop_frame;
%  trial2_start_frame, trial2_stop_frame;
%  ...
%  trialn_start_frame, trialn_stop_frame]
%
trials = coil_loader.computeTrials();


% get the frames where calibration events were detected
% matrix structured as such:
% [cal1_frame;
%  cal2_frame;
%  ...
%  caln_frame]
calibration_frames = coil_loader.computeCalibrationFrames();


% user tag frames
[user1_frames, user2_frames] = coil_loader.computeUserTagFrames();


%END
