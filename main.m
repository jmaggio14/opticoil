disp('importing settings from SETTINGS.m...');
run('SETTINGS.m');

% setup our figure
figure;


% prepare coil data collection
run('coilrecord');

% The daq is now setup to be triggered by Motive as soon as motive starts
% recording

% prepare video collection in another thread with our prebuilt video
% workspace
disp('launching the video recording thread...');
video_j = batch('recordvideo', 'Workspace', VIDEO_WORKSPACE);

% prepare the optitrack collection in another thread with our prebuilt opti
% workspace
disp('launching the optitrack recording thread...');
opti_j = batch('optitrackrecord','Workspace', OPTI_WORKSPACE);

disp('Matlab will prompt motive to start collecting data momentarily...')
start_t = posixtime( datetime( 'now' ) );
while true
    if isfile(THREAD_COMM_START_FILE)
        disp('recording in progress...');
        disp('once started, you may end this session by hitting ESC on the matlab figure');
        start_t = posixtime( datetime( 'now' ) );
        break
    end
    if (posixtime( datetime('now') ) - start_t) > EXTERNAL_TRIGGER_TIMEOUT
        s.stop();
        daq.reset()
        fclose(coil_fileID);
        fclose(tags_fileID);
        wait(opti_j);
        diary(opti_j);
        wait(video_j);
        diary(video_j);
        close;
        return
    end
end


while true
    w = waitforbuttonpress;
    if w
        key = get(gcf,'currentcharacter');
        if key == 27 % ESCAPE KEY
            % create a file to signify the worker thread that it should
            % stop running
            fid = fopen(THREAD_COMM_STOP_FILE,'w');
            fprintf(fid,'');
            fclose(fid);
            break;
        end
    end
end



s.stop();
daq.reset()
fclose(coil_fileID);
fclose(tags_fileID);
wait(opti_j);
diary(opti_j);
wait(video_j);
diary(video_j);
close;


% copy files to Opus using an ftp connection
disp('copying files to opus and server...');
ftpobj = ftp('128.151.171.182','aplab','Qebgcsft2018');

ftpobj.mkdir(DATE_DIR);
ftpobj.mkdir(UPLOAD_DIR);
ftpobj.cd(UPLOAD_DIR);

disp(['copying coil data to ',UPLOAD_DIR]);
ftpobj.mput(COIL_FILENAME);

disp(['copying tags data to ',UPLOAD_DIR]);
ftpobj.mput(TAGS_FILENAME);

disp(['copying collection settings to ',UPLOAD_DIR]);
ftpobj.mput(SETTINGS_FILENAME);
ftpobj.close();

% disp(['copying session video to ',UPLOAD_DIR]);
% ftpobj.mput(VIDEO_FILENAME);
% ftpobj.close();

% END
