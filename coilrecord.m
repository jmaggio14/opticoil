% open up files and write the column headers
coil_fileID = fopen(COIL_FILENAME,'w');
tags_fileID = fopen(TAGS_FILENAME,'w');

% coil header for csv file
coil_header = ['timestamps,',...
            'ref12,',...
            'ref16,',...
            'ref20,',...
            'left_eye,',...
            'right_eye,',...
            'side_helmet_coil,',...
            'back_helmet_coil',...
            '\n',
            ];
fprintf(coil_fileID, coil_header);

% tags header for csv file
tags_header = ['eyeprobe_tag,',...
                'start_tag,',...
                'stop_tag,',...
                'cal_tag,',...
                'user_tag1,',...
                'user_tag2,',...
                'user_tag3,',...
                'cam_exposure',...
                '\n',];
fprintf(tags_fileID, tags_header);



% initialize the daq
s = daq.createSession('ni');

% Apply our settings to the DAQ
s.ExternalTriggerTimeout = EXTERNAL_TRIGGER_TIMEOUT;
s.Rate = SAMPLE_RATE;
s.NotifyWhenDataAvailableExceeds = CALLBACK_LIMIT;
s.IsContinuous = 1; % continuous data collection


% Instantiate our data collection channels


%%%%%%%% INPUT %%%%%%%%%%%%
%%% ANALOG CHANNELS

% reference coil channels
[ref1, ref12_idx] = addAnalogInputChannel(s,DEVICE,REF12,'Voltage');
[ref2, ref16_idx] = addAnalogInputChannel(s,DEVICE,REF16,'Voltage');
[ref3, ref20_idx] = addAnalogInputChannel(s,DEVICE,REF20,'Voltage');
% eye coil channels
[le_ch, left_eye_idx] = addAnalogInputChannel(s,DEVICE,LEFT_EYE_COIL,'Voltage');
[re_ch, right_eye_idx] = addAnalogInputChannel(s,DEVICE,RIGHT_EYE_COIL,'Voltage');
% head coil channels
[sh_ch, side_head_idx] = addAnalogInputChannel(s,DEVICE,SIDE_HEAD_COIL,'Voltage');
[bh_ch, back_head_idx] = addAnalogInputChannel(s,DEVICE,BACK_HEAD_COIL,'Voltage');


%%% DIGITAL CHANNELS
% add the digital channels we need to record button presses and events
% optitrack sync signal (gated exposure time)
[cam_exp_ch, cam_exp_idx] = addDigitalChannel(s,DEVICE,CAMERA_EXPOSURE,'InputOnly');

% Experiment Tags
[eye_ch, eyeprobe_idx] =  addDigitalChannel(s,DEVICE,EYE_BUTTON,'InputOnly');
[start_ch, start_idx] = addDigitalChannel(s,DEVICE,START_BUTTON,'InputOnly');
[stop_ch, stop_idx] = addDigitalChannel(s,DEVICE,STOP_BUTTON,'InputOnly');
[cal_ch, cal_idx] = addDigitalChannel(s,DEVICE,CAL_BUTTON,'InputOnly');

%User Tags
[user1_ch, user1_idx] = addDigitalChannel(s,DEVICE,USER_BUTTON1,'InputOnly');
[user2_ch, user2_idx] = addDigitalChannel(s,DEVICE,USER_BUTTON2,'InputOnly');
[user3_ch, user3_idx] = addDigitalChannel(s,DEVICE,USER_BUTTON3,'InputOnly');

% setup the trigger
[tc,t_idx] = addTriggerConnection(s,'external',[DEVICE,TRIGGER_PORT],'StartTrigger');

% store the indices in a struct
idxs = struct;

% data (analog)
idxs.ref12 = ref12_idx;
idxs.ref16 = ref16_idx;
idxs.ref20 = ref20_idx;
idxs.left_eye = left_eye_idx;
idxs.right_eye = right_eye_idx;
idxs.side_head = side_head_idx;
idxs.back_head = back_head_idx;
% tags (digital)
idxs.cam_exp = cam_exp_idx;
idxs.eyeprobe = eyeprobe_idx;
idxs.start = start_idx;
idxs.stop = stop_idx;
idxs.cal = cal_idx;
idxs.user1 = user1_idx;
idxs.user2 = user2_idx;
idxs.user3 = user3_idx;



% analog saving callback
lh_save = s.addlistener('DataAvailable', ...
              @(src,event) savecoildata(src,event,idxs,coil_fileID));
% digital saving callback
lh_tags = s.addlistener('DataAvailable',...
              @(src,event) savecoiltags(src,event,idxs,tags_fileID));
% plotting / UI callback
lh_plot = s.addlistener('DataAvailable', ...
              @(src,event) plotcoildata(src,event,idxs,BEEP_SOUND));

%%%%%%%% OUTPUT %%%%%%%%%%%%
s.addAnalogOutputChannel(DEVICE, CAMERA_TRIGGER_OUT, 'Voltage')
% generate a FRAME_RATE Hz square wave to use as the camera trigger
trigger_wave = square( 2*pi*FRAME_RATE .* linspace(0, 1, s.Rate) );
trigger_wave = (trigger_wave + 1) * (3.3 / 2);
trigger_wave = reshape(trigger_wave,numel(trigger_wave),1);



% create a handler to trigger the optitrack frame capture
s.queueOutputData(trigger_wave);
lh_trigger = addlistener(s,'DataRequired', ...
			@(src,event) src.queueOutputData(trigger_wave));



% tell the daq to start listening for the trigger
s.startBackground();
disp('DAQ configured and setup...');

% we're ready to start
