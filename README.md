

## TO Collect Synced data:

    1) Start Motive, make sure it can connect to all cameras AND the esync

    2) In esync properties, make sure 'Source' is set to "Input1"

    3) In esync properties, set output 1 to 'RECORDING-GATE'. normal polarity

    4) open the "main.m" script in matlab

    5) Make sure Motive is in Capture mode

    6) Run main.m. It will take some time, but will ultimately start recording in motive
        (if it fails, it will print out instructions)

    7) Once recording starts, you may end recording by hitting ESC on the matlab figure
        this will stop collecting frames immediately 

## To Change Capture Parameters (for example: framerate)
    1) open main.m
    2) modify global variables, this will change variables in ALL FILES
        it's not reccomended to change the channel ids
        

## Motive must be set up with the following options:
### In the Esync Panel
#### Sync Input Settings
    Source: Input1
    Input Divider: 1
    Input Multiplier: 1
#### Output1:
    Enabled: True
    Type: Recording Gate
    Polarity: Normal


---------------------------------------------------------------------------
If you have any issues, try restarting Motive and Matlab.

If that fails, load the settings profile contained in this repo into Motive and try again

If the coil data fails to capture, but motive is recording:

    Set 'Sync Input Setting' to another setting (I used SMPTE Time Code)
    Set 'Sync Input Setting' to back to "Input 1"
    Check if the blue light above output1 on the esync is on




