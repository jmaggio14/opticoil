% open the file for this session
galvo_fid = fopen([SESSION_NAME,'.csv'], 'w');
fprintf(galvo_fid,'timestamps,galvo(V),angle(arcmin),frame_gate\n');


s = daq.createSession('ni');
s.Rate = GALVO_SAMPLE_RATE;
s.NotifyWhenDataAvailableExceeds = GALVO_CALLBACK_LIMIT;
s.IsContinuous = 1;
s.ExternalTriggerTimeout = EXTERNAL_TRIGGER_TIMEOUT;


% 
% %%%%%%%% INPUT %%%%%%%%%%%%
% [read_ch, galvo_idx] = addAnalogInputChannel(s,DEVICE,GAVLO_READ,'Voltage');
% 
% 
% % add the digital channels we need to record optitrack sync signal (gated exposure time)
% [cam_exp_ch, cam_exp_idx] = addDigitalChannel(s,DEVICE,CAMERA_EXPOSURE,'InputOnly');
% 
% lh_save = s.addlistener('DataAvailable', ...
%               @(src,event) savegalvo(src, event, galvo_fid, galvo_idx, cam_exp_idx, GALVO_VOLTS_PER_ARCMIN) );
% % lh_plot = s.addlistener('DataAvailable', ...
% %               @(src,event) plotgalvo(src, event, galvo_idx, cam_exp_idx, GALVO_VOLTS_PER_ARCMIN));
% %                

%%%%%%% OUTPUT %%%%%%%%%%%%
[driver_ch, driver_idx] = s.addAnalogOutputChannel(DEVICE, GALVO_DRIVER_TRIGGER, 'Voltage');
% generate a OSCILLATION_FREQUENCY Hz square wave to push into the galvo
% driver 
galvo_driver = sin( 2*pi*GALVO_OSCILLATION_FREQUENCY .* linspace(0, 1, s.Rate) );
galvo_driver = galvo_driver * GALVO_DRIVER_AMPLITUDE;
galvo_driver = reshape(galvo_driver,numel(galvo_driver),1);



          
          
%%%%%%%% camera control %%%%%%%%%%%%
[cam_ch, cam_idx] = s.addAnalogOutputChannel(DEVICE, CAMERA_TRIGGER_OUT, 'Voltage');
% generate a FRAME_RATE Hz square wave to use as the camera trigger
cam_trig = square( 2*pi*FRAME_RATE .* linspace(0, 1, s.Rate) );
cam_trig = (cam_trig + 1) * (3.3 / 2);
cam_trig = reshape(cam_trig,numel(cam_trig),1);

% create a handler to trigger the optitrack frame capture
output = [galvo_driver, cam_trig];
s.queueOutputData(output);
addlistener(s,'DataRequired', ...
			@(src,event) src.queueOutputData(output));



        
        
%%%%%%%%%%%%%%%% SETTING UP THE START TRIGGER %%%%%%%%%%%%%%%%%%%%%%%
% setup the trigger
[tc,t_idx] = addTriggerConnection(s,'external',[DEVICE,TRIGGER_PORT],'StartTrigger');
    

s.startBackground();
disp('DAQ configured and setup...');


