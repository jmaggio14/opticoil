# Copyright (c) Active Perception Lab, University of Rochester
# Author: Jeff Maggio 08/30/2019

import sys
import json
import time
from collections import namedtuple, deque
from threading import Event

from AplabNatNetClient import NatNetClient


FrameMetadata = namedtuple('FrameMetadata',
                                            ['frameNumber',
                                            'markerSetCount',
                                            'unlabeledMarkersCount',
                                            'rigidBodyCount',
                                            'skeletonCount',
                                            'labeledMarkerCount',
                                            'timecode',
                                            'timecodeSub',
                                            'timestamp',
                                            'isRecording',
                                            'trackedModelsChanged',
                                            ],
                                            )

RigidBody = namedtuple('RigidBody',
                                ['rb_id',
                                'rb_position',
                                'rb_orientation',
                                'mean_error',
                                'is_tracking'],
                                    )

RigidBodyDescription = namedtuple('RigidBodyDescription',
                                        ['rb_id',
                                        'markers',
                                        'marker_labels']
                                        )


def timer(func):
    def _timer(*args,**kwargs):
        start = time.time()
        ret = func(*args,**kwargs)
        print( round(1000*(time.time() - start),6),'ms' )

        return ret
    return _timer

class OptitrackRecorder():
    def __init__(self, filename):
        self.filename = filename
        self.current_frame = None


        self.frames_metadata = deque(maxlen=1728000)
        self.frames_rigidbody = deque(maxlen=1728000)
        self.rigid_body_descriptions = deque()

        self.stop_event = Event()

        print("data will be saved to %s" % filename)

    # @timer
    def new_frame(self,
                    frameNumber,
                    markerSetCount,
                    unlabeledMarkersCount,
                    rigidBodyCount,
                    skeletonCount,
                    labeledMarkerCount,
                    timecode,
                    timecodeSub,
                    timestamp,
                    isRecording,
                    trackedModelsChanged
                    ):

        if self.current_frame is None:
            print('beginning data collection...')

        elif self.current_frame > frameNumber:
            self.close("playback restarted, ending data collection.")
            return

        self.current_frame = frameNumber

        metadata = FrameMetadata(frameNumber,
                        markerSetCount,
                        unlabeledMarkersCount,
                        rigidBodyCount,
                        skeletonCount,
                        labeledMarkerCount,
                        timecode,
                        timecodeSub,
                        timestamp,
                        isRecording,
                        trackedModelsChanged)

        self.frames_metadata.append( metadata )

    def new_rigid_body(self, rb_id, rb_position, rb_orientation, mean_error, is_tracking):
        rb = RigidBody(rb_id, rb_position, rb_orientation, mean_error, is_tracking)

        #append a tuple containing the frame number and the rigid body data
        self.frames_rigidbody.append( (self.current_frame, rb) )

    def new_rigid_body_description(self, rb_id, markers, marker_labels):

        print('found rigid body %s' % rb_id)
        rb_des = RigidBodyDescription(rb_id, markers, marker_labels)

        self.rigid_body_descriptions.append( rb_des )

    def __del__(self):
        self.close("recorder object deleted")

    def close(self, msg):
        print(msg)
        print('saving data to %s. DO NOT STOP THE PROCESS YET!' % self.filename)

        all_data = {'relative_marker_position':{},
                    'frames':{}}

        # add the rigid body marker positions
        for rb_des in self.rigid_body_descriptions:
            all_data['relative_marker_position'][str(rb_des.rb_id)] = \
                                dict( zip(map(str,rb_des.marker_labels),rb_des.markers) )

        # add all frames to the data
        num_frames =  len(self.frames_metadata)
        approx_missed = self.frames_metadata[-1].frameNumber - num_frames
        print('%s (%s%%)frames dropped during capture. if this number is signifcant try reducing playback speed in Motive' % (approx_missed, round(approx_missed/num_frames * 100,1)))

        for frame in self.frames_metadata:
            all_data['frames'][str(frame.frameNumber)] = dict( frame._asdict() )
            all_data['frames'][str(frame.frameNumber)]['rigid_bodies'] = {}

        # go through and sort all rigid bodies into the appropriate frame
        for frame_id,rb in self.frames_rigidbody:
            rb_data = {}
            rb_data['pos'] = tuple(rb.rb_position)
            rb_data['quat'] = tuple(rb.rb_orientation)
            rb_data['mean_error'] = float(rb.mean_error)
            rb_data['is_tracking'] = bool(rb.is_tracking)

            if frame_id is None:
                frame_id = str(self.frames_metadata[0].frameNumber)
            all_data['frames'][str(frame_id)]['rigid_bodies'][str(rb.rb_id)] = rb_data

        with open(self.filename,'w') as fp:
            json.dump(all_data, fp)

        # trigger the child threads to close
        self.stop_event.set()

        print('data saving complete')



if __name__ == "__main__":
    # fetch the filename from the command line args
    # get a filename from the terminal argument
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('filename')
    args = parser.parse_args()

    # create our recorder object
    recorder = OptitrackRecorder(args.filename)

    # This will create a new NatNet client
    streamingClient = NatNetClient(recorder.stop_event)

    # Configure the streaming client to call our rigid body handler on the emulator to send data out.
    streamingClient.aplab_new_frame = recorder.new_frame
    streamingClient.aplab_new_rb_description = recorder.new_rigid_body_description
    streamingClient.aplab_new_rb = recorder.new_rigid_body

    # Start up the streaming client now that the callbacks are set up.
    # This will run perpetually, and operate on a separate thread until the
    # recorder detects that playback has restarted
    streamingClient.run()

    while not recorder.stop_event.is_set():
        pass
