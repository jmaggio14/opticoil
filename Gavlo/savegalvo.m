function [] = savegalvo(src,event,fid, galvo_idx, cam_exp_idx, VOLTS_PER_ARCMIN)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

raw_galvo = event.Data(:,galvo_idx);
angle = raw_galvo / VOLTS_PER_ARCMIN;
frame_gate = event.Data(:,cam_exp_idx);

data = [event.TimeStamps, raw_galvo, angle, frame_gate];
fprintf(fid,'%d,%d,%d,%d\n',data');

end

