addpath('..');
natnetclient = natnet;

natnetclient.HostIP = '127.0.0.1';
natnetclient.ClientIP = '127.0.0.1';
natnetclient.ConnectionType = 'Multicast';

% connect and promptly stop all recording and playback
% natnetclient.connect();
natnetclient.IsReporting = true;

% connect and start recording
% natnetclient.editMode();
% natnetclient.liveMode();

natnetclient.connect();
pause(1);
natnetclient.setTakeName(TAKE_NAME);
natnetclient.startRecord();

% create a file to signify to main thread that this thread is ready
if natnetclient.IsConnected
    fid = fopen(THREAD_COMM_START_FILE,'w');
    fprintf(fid,'');
    fclose(fid);
    disp('connection establed, recording begun');
else
    disp('unable to establish a connection with motive');
    disp('Is motive in capture mode? maybe try restarting it');
    return
end
    

while true
   % don't end recording until we get a message from another thread
    if isfile(THREAD_COMM_STOP_FILE)
        break
    end
end

natnetclient.stopRecord();
natnetclient.disconnect();
delete(natnetclient);
