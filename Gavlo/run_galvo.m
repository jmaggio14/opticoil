% disp('importing settings from SETTINGS.m...');
run('../SETTINGS.m');
addpath('..');
% 
% 
% setup the thread communication file
if isfile(THREAD_COMM_START_FILE)
    delete(THREAD_COMM_START_FILE);
end
if isfile(THREAD_COMM_STOP_FILE)
    delete(THREAD_COMM_STOP_FILE);
end

% prepare galvo data collection
run('galvorecord');

% The daq is now setup to be triggered by Motive as soon as motive starts
% recording

% prepare the optitrack collection in another thread with our prebuilt opti
% workspace
disp('launching the optitrack recording thread...');
opti_j = batch('optitrackrecord','Workspace', OPTI_WORKSPACE);
% opti_j = batch('test');

disp('Matlab will prompt motive to start collecting data momentarily...')
start_t = posixtime( datetime( 'now' ) );
while true
    if isfile(THREAD_COMM_START_FILE)
        disp('recording in progress...');
        disp('once started, you may end this session by hitting ESC on the matlab figure');
        start_t = posixtime( datetime( 'now' ) );
        break
    end
    if (posixtime( datetime('now') ) - start_t) > EXTERNAL_TRIGGER_TIMEOUT
        s.stop();
        daq.reset()
        fclose(galvo_fid);
        wait(opti_j);
        diary(opti_j);
        close;
        return
    end
end


while true
    w = waitforbuttonpress;
    if w
        key = get(gcf,'currentcharacter');
        if key == 27 % ESCAPE KEY
            % create a file to signify the worker thread that it should
            % stop running
            fid = fopen(THREAD_COMM_STOP_FILE,'w');
            fprintf(fid,'');
            fclose(fid);
            break;
        end
    end
end



s.stop();
daq.reset()
fclose(galvo_fid);
wait(opti_j);
diary(opti_j);
close;


% % copy files to Opus using an ftp connection
% disp('copying files to opus and server...');
% ftpobj = ftp('128.151.171.182','aplab','Qebgcsft2018');
% 
% ftpobj.mkdir(DATE_DIR);
% ftpobj.mkdir(UPLOAD_DIR);
% ftpobj.cd(UPLOAD_DIR);
% 
% disp(['copying coil data to ',UPLOAD_DIR]);
% ftpobj.mput(COIL_FILENAME);
% 
% disp(['copying tags data to ',UPLOAD_DIR]);
% ftpobj.mput(TAGS_FILENAME);
% 
% disp(['copying collection settings to ',UPLOAD_DIR]);
% ftpobj.mput(SETTINGS_FILENAME);
% ftpobj.close();
% 
% disp(['copying session video to ',UPLOAD_DIR]);
% ftpobj.mput(VIDEO_FILENAME);
% ftpobj.close();

% END
